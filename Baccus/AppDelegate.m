//
//  AppDelegate.m
//  Baccus
//
//  Created by David de Tena on 03/03/16.
//  Copyright © 2016 David de Tena. All rights reserved.
//

#define IS_IPHONE UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone

#import "AppDelegate.h"
#import "DTCWineryModel.h"
#import "DTCWineryTableViewController.h"
#import "DTCWineModel.h"
#import "DTCWineViewController.h"
#import "DTCWebViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Create model, controller and navigation
    DTCWineryModel *winery = [[DTCWineryModel alloc] init];
    
    // Check if current device is phone or pad
    UIViewController *rootVC = nil;
    if(!IS_IPHONE){
        // Setup for iPad
        rootVC = [self configureRootViewControllerForPadWithModel: winery];
    }
    else{
        // Setup for iPhone
        rootVC = [self configureRootViewControllerForPhoneWithModel: winery];
    }
    
    // Root controller
    self.window.rootViewController = rootVC;
    
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Utils
// Last selected wine will be loaded as default for pads
- (UIViewController *) configureRootViewControllerForPadWithModel:(DTCWineryModel *) model{
    DTCWineryTableViewController *wineryTVC = [[DTCWineryTableViewController alloc] initWithModel:model
                                                                                            style:UITableViewStyleGrouped];
    // First wine to be displayed is the one stored into NSUserDefaults
    DTCWineViewController *wineVC = [[DTCWineViewController alloc] initWithModel: [wineryTVC lastSelectedWine]];
    
    // Create combiners for the table and for the first wine
    UINavigationController *wineryNav = [[UINavigationController alloc] initWithRootViewController:wineryTVC];
    UINavigationController *wineNav = [[UINavigationController alloc] initWithRootViewController:wineVC];
    
    // Create SplitVC with its delegate
    UISplitViewController *splitVC = [[UISplitViewController alloc] init];
    [splitVC setViewControllers:@[wineryNav,wineNav]];
    
    // Set delegates
    // WineVC is the delegate for both the SplitVC and WineryTableVC
    splitVC.delegate = wineVC;
    wineryTVC.delegate = wineVC;

    // SplitVC is the rootViewController for ipads
    return splitVC;
}

// Embed table in a NavigationController for phone-based screens
- (UIViewController *) configureRootViewControllerForPhoneWithModel:(DTCWineryModel *) model{
    DTCWineryTableViewController *wineryTVC = [[DTCWineryTableViewController alloc] initWithModel:model
                                                                                            style:UITableViewStyleGrouped];
    
    UINavigationController *wineryNav = [[UINavigationController alloc] initWithRootViewController:wineryTVC];
    wineryTVC.delegate = wineryTVC;
    return wineryNav;
}


@end
