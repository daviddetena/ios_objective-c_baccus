//
//  DTCWineryTableViewController.m
//  Baccus
//
//  Created by David de Tena on 04/03/16.
//  Copyright © 2016 David de Tena. All rights reserved.
//

#import "DTCWineryTableViewController.h"
#import "DTCWineModel.h"
#import "DTCWineViewController.h"

@interface DTCWineryTableViewController ()

@end

@implementation DTCWineryTableViewController

#pragma mark - Init
- (id) initWithModel:(DTCWineryModel *) aModel
               style:(UITableViewStyle) aStyle{

    if(self = [super initWithStyle:aStyle]){
        _model = aModel;
        self.title = @"Baccus";
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == RED_WINE_SECTION) {
        return self.model.redWineCount;
    }
    else if (section == WHITE_WINE_SECTION){
        return self.model.whiteWineCount;
    }
    else{
        return self.model.otherWineCount;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    // Empty cell => create a new one
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellIdentifier];
    }
    
    // Find out the proper model
    DTCWineModel *wine = [self wineAtIndexPath:indexPath];
    
    // Sync cell (view) with model
    cell.textLabel.text = wine.name;
    cell.imageView.image = wine.photo;
    cell.detailTextLabel.text = wine.wineCompanyName;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == RED_WINE_SECTION) {
        return @"Red wines";
    }
    else if (section == WHITE_WINE_SECTION){
        return @"White wines";
    }
    else{
        return @"Other wines";
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section{

    int number = 0;
    
    if (section == RED_WINE_SECTION) {
        number = [self.model redWineCount];
    }
    else if (section == WHITE_WINE_SECTION){
        number = [self.model whiteWineCount];
    }
    else{
        number = [self.model otherWineCount];
    }
    
    if(number == 1){
        return @"1 wine";
    }
    else{
        NSString *footer = [NSString stringWithFormat:@"%d wines", number];
        return footer;
    }
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    // Assuming that we have an UINavigationController, find out the wine selected
    DTCWineModel *wine = [self wineAtIndexPath:indexPath];
    
    // The table tells its delegate that the wine selected has changed
    // Broadcast changes using notifications
    [self.delegate wineryTableViewController:self
                               didSelectWine:wine];
    
    NSNotification *not = [NSNotification notificationWithName:NEW_WINE_NOTIFICATION_NAME
                                                        object:self
                                                      userInfo:@{WINE_KEY:wine}];
    
    [[NSNotificationCenter defaultCenter] postNotification:not];
    
    // Save last selected wine
    [self saveLastSelectedWineAtSection:indexPath.section
                                    row:indexPath.row];
}



#pragma mark - Utils 
- (DTCWineModel *) wineAtIndexPath: (NSIndexPath *)indexPath{
    DTCWineModel *wine = nil;
    
    if(indexPath.section == RED_WINE_SECTION){
        // Red wine section
        wine = [self.model redWineAtIndex:indexPath.row];
    }
    else if (indexPath.section == WHITE_WINE_SECTION){
        // White wine section
        wine = [self.model whiteWineAtIndex:indexPath.row];
    }
    else{
        // Other wine section
        wine = [self.model otherWineAtIndex:indexPath.row];
    }
    
    return wine;
}


#pragma mark - NSUserDefaults

- (NSDictionary *) setDefaults{

    // First wine of redWines is the default wine store into NSUserDefaults
    NSDictionary *defaultWineCoords = @{SECTION_KEY: @(RED_WINE_SECTION), ROW_KEY: @0};
    
    NSUInteger section = RED_WINE_SECTION;
    NSUInteger row = 0;
    
    [self saveLastSelectedWineAtSection:section row:row];
    
    return defaultWineCoords;
}


- (void) saveLastSelectedWineAtSection:(NSUInteger) aSection row:(NSUInteger) aRow{
    // Every time a new wine is tapped, we store into NSUserDefaults as the last selected wine
    NSDictionary *defaultWineCoords = @{SECTION_KEY: @(aSection), ROW_KEY: @(aRow)};
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:defaultWineCoords forKey:LAST_WINE_KEY];
    [defaults synchronize];

}


- (DTCWineModel *) lastSelectedWine{

    NSIndexPath *indexPath = nil;
    NSDictionary *coords = nil;
    
    // Get coords stored into NSUserDefaults
    coords = [[NSUserDefaults standardUserDefaults] objectForKey:LAST_WINE_KEY];
    
    if (coords == nil) {
        // No previous data. Set current selected wine as default
        coords = [self setDefaults];
    }
    
    // Transform indexPath from NSUserDefault
    indexPath = [NSIndexPath indexPathForRow:[[coords objectForKey:ROW_KEY] integerValue]
                                   inSection:[[coords objectForKey:SECTION_KEY] integerValue]];
    
    // Get the wine for that indexPath
    return [self wineAtIndexPath:indexPath];
}


#pragma mark - WineryTableViewControllerDelegate
// Auto-delegation for phones. Table needs to know by itself that a new wine has been selected in order to push
// a new VC to it
-(void) wineryTableViewController:(DTCWineryTableViewController *)wineryVC didSelectWine:(DTCWineModel *)aWine{
    
    // Push to new WineVC
    DTCWineViewController *wineVC = [[DTCWineViewController alloc] initWithModel:aWine];
    [self.navigationController pushViewController:wineVC animated:YES];
}

@end
