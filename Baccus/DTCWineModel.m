//
//  DTCWineModel.m
//  Baccus
//
//  Created by David de Tena on 03/03/16.
//  Copyright © 2016 David de Tena. All rights reserved.
//

#import "DTCWineModel.h"

@implementation DTCWineModel

// When a read-only property is created and a custom getter is implemented, like in this case,
// the compiler assumes that you don't need an instance variable for that. In this case, we do
// need that instance variable, so we need to force the compiler to include it. This is made by
// means of @synthesize. We tell the compiler that we want a property called 'photo' with an
// instance variable called _photo.
// This is optional in most of cases.
@synthesize photo = _photo;

#pragma mark - Properties
// Our custom getter for photo (readonly). We set an image with the data from the photoURL in
// the JSON
-(UIImage *) photo{
    
    // TODO: fix that so that will be done in background
    // Lazy load: image is loaded only if needed
    if(_photo == nil){
        _photo = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.photoURL]];
    }
    return _photo;
}

#pragma mark - Class init
// Designated class initializer
+(id) initWithName: (NSString *) aName
   wineCompanyName: (NSString *) aWineCompanyName
              type: (NSString *) aType
            origin: (NSString *) anOrigin
            grapes: (NSArray *) arrayOfGrapes
    wineCompanyWeb: (NSURL *) anURL
             notes: (NSString *) aNotes
            rating: (int) aRating
          photoURL: (NSURL *) aPhotoURL{
    
    return [[self alloc] initWithName:aName
                      wineCompanyName:aWineCompanyName
                                 type:aType
                               origin:anOrigin
                               grapes:arrayOfGrapes
                       wineCompanyWeb:anURL
                                notes:aNotes
                               rating:aRating
                             photoURL:aPhotoURL];
}

// Convenience
+(id) initWithName: (NSString *) aName
   wineCompanyName: (NSString *) aWineCompanyName
              type: (NSString *) aType
            origin: (NSString *) anOrigin{
    
    return [[self alloc] initWithName:aName
                      wineCompanyName:aWineCompanyName
                                 type:aType
                               origin:anOrigin];
}


#pragma mark - Init
// Designated initializer
-(id) initWithName: (NSString *) aName
   wineCompanyName: (NSString *) aWineCompanyName
              type: (NSString *) aType
            origin: (NSString *) anOrigin
            grapes: (NSArray *) arrayOfGrapes
    wineCompanyWeb: (NSURL *) anURL
             notes: (NSString *) aNotes
            rating: (int) aRating
          photoURL: (NSURL *) aPhotoURL{

    if(self = [super init]){
        _name = aName;
        _wineCompanyName = aWineCompanyName;
        _type = aType;
        _origin = anOrigin;
        _grapes = arrayOfGrapes;
        _wineCompanyWeb = anURL;
        _notes = aNotes;
        _rating = aRating;
        _photoURL = aPhotoURL;
    }
    return self;
}

// Convenience initializers
-(id) initWithName: (NSString *) aName
   wineCompanyName: (NSString *) aWineCompanyName
              type: (NSString *) aType
            origin: (NSString *) anOrigin{
    
    return [self initWithName:aName
              wineCompanyName:aWineCompanyName
                         type:aType
                       origin:anOrigin
                       grapes:nil
               wineCompanyWeb:nil
                        notes:nil
                       rating:NO_RATING
                     photoURL:nil];
}


#pragma mark - JSON

// Extract JSON info about a wine and turn into a DTCWineModel object
-(id) initWithDictionary:(NSDictionary *)aDict{
    return [self initWithName:[aDict objectForKey:@"name"]
              wineCompanyName:[aDict objectForKey:@"company"]
                         type:[aDict objectForKey:@"type"]
                       origin:[aDict objectForKey:@"origin"]
                       grapes:[self extractGrapesFromJSONArray:[aDict objectForKey:@"grapes"]]
               wineCompanyWeb:[NSURL URLWithString:[aDict objectForKey:@"company_web"]]
                        notes:[aDict objectForKey:@"notes"]
                       rating:[[aDict objectForKey:@"rating"] intValue]
                     photoURL:[NSURL URLWithString:[aDict objectForKey:@"picture"]]];
}

// Pack a DTCWineModel object into JSON
-(NSDictionary *) proxyForJSON{
    return @{@"name": self.name,
             @"company": self.wineCompanyName,
             @"company_web":[self.wineCompanyWeb absoluteString],
             @"type":self.type,
             @"origin":self.origin,
             @"grapes": [self packGrapesIntoJSONArray],
             @"notes":self.notes,
             @"rating":@(self.rating),
             @"picture":[self.photoURL path]};
}


#pragma mark - Utils

// Extract each grape from the array of dictionaries in JSON
- (NSArray *) extractGrapesFromJSONArray:(NSArray *) JSONArray{
    NSMutableArray *grapes = [NSMutableArray arrayWithCapacity:[JSONArray count]];
    for (NSDictionary *dict in JSONArray) {
        [grapes addObject:[dict objectForKey:@"grape"]];
    }
    return grapes;
}

// Pack in an array of dictionaries all the grapes of a wine
- (NSArray *) packGrapesIntoJSONArray{
    NSMutableArray *JSONArray = [NSMutableArray arrayWithCapacity:[self.grapes count]];
    for (NSString *grape in self.grapes) {
        // One dictionary per grape
        [JSONArray addObject:@{@"grape":grape}];
    }
    return JSONArray;
}

@end
