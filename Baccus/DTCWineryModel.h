//
//  DTCWineryModel.h
//  Baccus
//
//  Created by David de Tena on 03/03/16.
//  Copyright © 2016 David de Tena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DTCWineModel.h"

#define RED_WINE_KEY    @"Tinto"
#define WHITE_WINE_KEY  @"Blanco"
#define OTHER_WINE_KEY  @"Rosado"
#define REMOTE_JSON     @"https://dl.dropboxusercontent.com/u/66300733/App-Resources/ios-baccus.json"
#define LOCAL_JSON      @"baccus.json"


@interface DTCWineryModel : NSObject

#pragma mark - Properties
@property (nonatomic, readonly) int redWineCount;
@property (nonatomic, readonly) int whiteWineCount;
@property (nonatomic, readonly) int otherWineCount;


# pragma mark - Utils
- (DTCWineModel *) redWineAtIndex:(int) index;
- (DTCWineModel *) whiteWineAtIndex:(int) index;
- (DTCWineModel *) otherWineAtIndex:(int) index;


@end
