//
//  DTCWineryTableViewController.h
//  Baccus
//
//  Created by David de Tena on 04/03/16.
//  Copyright © 2016 David de Tena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DTCWineryModel.h"

#define RED_WINE_SECTION 0
#define WHITE_WINE_SECTION 1
#define OTHER_WINE_SECTION 2

#define NEW_WINE_NOTIFICATION_NAME @"newWine"
#define WINE_KEY @"wine"

#define LAST_WINE_KEY @"lastWine"
#define SECTION_KEY @"section"
#define ROW_KEY @"row"


// PUT @class for imports of own classes
// Forward declaration of current class
@class DTCWineryTableViewController;

@protocol WineryTableViewControllerDelegate <NSObject>

// Optional Methods
@optional
- (void) wineryTableViewController:(DTCWineryTableViewController *) wineryVC didSelectWine:(DTCWineModel *) aWine;


// Required Methods
@required


@end

@interface DTCWineryTableViewController : UITableViewController<WineryTableViewControllerDelegate>

#pragma mark - Properties 

@property (nonatomic, strong) DTCWineryModel *model;
@property (nonatomic, weak) id<WineryTableViewControllerDelegate> delegate;


#pragma mark - Init 
- (id) initWithModel:(DTCWineryModel *) aModel
               style:(UITableViewStyle) aStyle;

#pragma mark - Utils
- (DTCWineModel *) lastSelectedWine;


@end