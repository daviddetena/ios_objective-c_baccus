//
//  DTCWebViewController.m
//  Baccus
//
//  Created by David de Tena on 03/03/16.
//  Copyright © 2016 David de Tena. All rights reserved.
//

#import "DTCWebViewController.h"
#import "DTCWineryTableViewController.h"

@interface DTCWebViewController ()

@end

@implementation DTCWebViewController


- (id) initWithModel:(DTCWineModel *) aModel{
    if(self = [super initWithNibName:nil bundle:nil]){
        _model = aModel;
        self.title = @"Web";
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self displayURL: self.model.wineCompanyWeb];
    
    // Suscribe for notifications
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(wineDidChange:)
                   name:NEW_WINE_NOTIFICATION_NAME
                 object:nil];
}


- (void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    // Unsuscribe from notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Utils
- (void) displayURL: (NSURL *) aURL{
    // Make this VC WebView's delegate to understand their methods
    self.browser.delegate = self;
    self.activityView.hidden = NO;
    [self.activityView startAnimating];
    [self.browser loadRequest:[NSURLRequest requestWithURL:aURL]];
}


#pragma mark - UIWebViewDelegate
- (void) webViewDidFinishLoad:(UIWebView *)webView{
    [self.activityView stopAnimating];
    self.activityView.hidden = YES;
}


#pragma mark - Notifications
- (void) wineDidChange:(NSNotification *) info{
    // Extract wine selected from userInfo
    NSDictionary *userInfo = [info userInfo];
    DTCWineModel *newWine = [userInfo objectForKey:WINE_KEY];
    
    // Update this model with the model (wine) in the notification info
    self.model = newWine;
    self.title = newWine.name;
    
    // Now the web displayed is the one from the new wine selected
    [self displayURL:newWine.wineCompanyWeb];
}


@end
