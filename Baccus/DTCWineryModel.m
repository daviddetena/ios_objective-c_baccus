//
//  DTCWineryModel.m
//  Baccus
//
//  Created by David de Tena on 03/03/16.
//  Copyright © 2016 David de Tena. All rights reserved.
//

#import "DTCWineryModel.h"

@interface DTCWineryModel()

// Declare "private" properties and methods
@property (strong, nonatomic) NSMutableArray *redWines;
@property (strong, nonatomic) NSMutableArray *whiteWines;
@property (strong, nonatomic) NSMutableArray *otherWines;

@end


@implementation DTCWineryModel

#pragma mark - Properties

// Custom getters of read-only properties
-(int) redWineCount{
    return (int)[self.redWines count];
}

-(int) whiteWineCount{
    return (int)[self.whiteWines count];
}

-(int) otherWineCount{
    return (int)[self.otherWines count];
}


#pragma mark - Init

// Create the model of Winery when initializing the object
-(id) init{
    if(self = [super init]){
        
        // Check if there is a local .json file with data. If so, load data and images from Sandbox
        BOOL shouldDownloadData = NO;
        NSError *error = nil;
        NSData *data = nil;
        NSURL *localJSON = [self localJSONUrl];
        
        if(localJSON != nil){
            // Get wines from local JSON
            data = [NSData dataWithContentsOfURL:localJSON options:NSDataReadingUncached error:&error];
        }
        else{
            // Get wines from remote JSON
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:REMOTE_JSON]];
            // TODO: replace NSURLConnection with NSURLSession
            NSURLResponse *response = [[NSURLResponse alloc] init];
            data = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            shouldDownloadData = YES;
        }
        
        if(data != nil){
            // No error => get JSON objects
            NSArray *JSONObjects = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            NSMutableArray *newJSONObjects = [NSMutableArray arrayWithCapacity:[JSONObjects count]];
            
            if(JSONObjects != nil){
                // No error =>  turn each wine dictionary into DTCWineModel object by means of our custom init
                for (NSDictionary *dict in JSONObjects) {
                    DTCWineModel *wine = [[DTCWineModel alloc] initWithDictionary:dict];
                    
                    if ([wine.type isEqualToString:RED_WINE_KEY]) {
                        // Wine type: red => add to the proper array
                        if (!self.redWines) {
                            self.redWines = [NSMutableArray arrayWithObject:wine];
                        }
                        else{
                            [self.redWines addObject:wine];
                        }
                    } else if ([wine.type isEqualToString:WHITE_WINE_KEY]){
                        // Wine type: white => add to the proper array
                        if (!self.whiteWines) {
                            self.whiteWines = [NSMutableArray arrayWithObject:wine];
                        }
                        else{
                            [self.whiteWines addObject:wine];
                        }
                    }
                    else{
                        // Wine type: other => add to the proper array
                        if (!self.otherWines) {
                            self.otherWines = [NSMutableArray arrayWithObject:wine];
                        }
                        else{
                            [self.otherWines addObject:wine];
                        }
                    }
                    NSLog(@"Picture URL: %@", wine.photoURL);
                    
                    if (shouldDownloadData) {
                        // Download image to caches
                        [self saveImageToSandboxFromRemoteURL: wine.photoURL];
                    }
                    // Get photo url from local
                    wine.photoURL = [self URLForImageWithFileName: [wine.photoURL lastPathComponent]];
                    
                    // Generate new dictionary from the model object
                    NSDictionary *wineDict = [wine proxyForJSON];
                    
                    // Append each new JSON data to the JSON array
                    [newJSONObjects addObject:wineDict];
                }
                
                if (shouldDownloadData) {
                    // Save array of NSData to JSON
                    [self saveModelToJSONWithData:newJSONObjects];
                }
            }
            else{
                // Error
                NSLog(@"Error while parsing JSON: %@", error.localizedDescription);
            }
        }
        else{
            // Error
            NSLog(@"Error while downloading JSON from server: %@", error.localizedDescription);
        }
        
    }
    return self;
}


# pragma mark - Utils
- (DTCWineModel *) redWineAtIndex:(int) index{
    return (DTCWineModel *) [self.redWines objectAtIndex:index];
}

- (DTCWineModel *) whiteWineAtIndex:(int) index{
    return (DTCWineModel *) [self.whiteWines objectAtIndex:index];
}

- (DTCWineModel *) otherWineAtIndex:(int) index{
    return (DTCWineModel *) [self.otherWines objectAtIndex:index];
}


#pragma mark - JSON

// Check if we need to download JSON or was previously downloaded
- (NSURL *) localJSONUrl{
    // Check if there is a .json in Sandbox
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *jsonPath = [[self defaultCacheFolder] URLByAppendingPathComponent:LOCAL_JSON];
    
    if(![fm fileExistsAtPath:[jsonPath path]]){
        return nil;
    }
    else{
        return jsonPath;
    }
}


#pragma mark - Sandbox

- (NSURL *) URLForImageWithFileName:(NSString *) filename{
    NSURL *imageURL = [[self defaultImageFolder] URLByAppendingPathComponent:filename];
    return imageURL;
}


// Save remote image from wine into local url in sandbox
- (void) saveImageToSandboxFromRemoteURL:(NSURL *) aURL{
    
    NSError *error;
    NSData *imageData = [NSData dataWithContentsOfURL:aURL options:kNilOptions error:&error];
    
    if(imageData == nil){
        // Read error
        NSLog(@"Error while retrieving image from server... %@", error.localizedDescription);
    }
    else{
        //  Downloaded => Save to Sandbox
        NSString *fileName = [aURL lastPathComponent];
        NSURL *localURL = [[self defaultImageFolder] URLByAppendingPathComponent:fileName];
        
        BOOL res = NO;
        res = [imageData writeToURL:localURL options:NSDataWritingAtomic error:&error];
        
        // Write error
        if(res == NO){
            NSLog(@"Error while saving to Sandbox... %@", error.localizedDescription);
        }
    }
}

- (void) saveModelToJSONWithData:(NSArray *) data{
    
    // Check if baccus.json exists in Cache folder. Create if not
    NSError *error = nil;
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *jsonPath = [[self defaultCacheFolder] URLByAppendingPathComponent:LOCAL_JSON];
    
    if(![fm fileExistsAtPath:[jsonPath path]]){
        // File does not exist. Create one
        NSLog(@"%@ does not exist. Let's create...", LOCAL_JSON);
        
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&error];
        BOOL res = NO;
        res = [jsonData writeToURL:jsonPath options:NSDataWritingAtomic error:&error];
        
        // Write error
        if(res == NO){
            NSLog(@"Error while saving JSON to Sandbox... %@", error.localizedDescription);
        }
    }
    else{
       NSLog(@"%@ already does exist.", LOCAL_JSON);
    }
}


- (NSURL *) defaultCacheFolder{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *cacheFolder = [[fm URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
    
    return cacheFolder;
}


- (NSURL *) defaultImageFolder{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *imagePath = [[self defaultCacheFolder] URLByAppendingPathComponent:@"images"];
    
    NSError *error = nil;
    if(![fm fileExistsAtPath:[imagePath path]]){
        // Directory does not exist. Create one
        NSLog(@"images/ folder does not exist. Let's create...");
        
        if (![fm createDirectoryAtURL:imagePath
          withIntermediateDirectories:YES
                           attributes:nil
                                error:&error]){
            // Handle the error.
            NSLog(@"Error when creating directory images/: %@", error.localizedDescription);
            return nil;
        }
    }
    return imagePath;
}


@end
